export const getItem = (key) => {
  try {
    return localStorage.getItem(key);
  } catch (error) {
    return null;
  }
};

export const setItem = (key, data) => {
  try {
    localStorage.setItem(key, data);
  } catch (error) {
    return null;
  }
};

export const removeItem = (key) => {
  try {
    localStorage.removeItem(key);
  } catch (error) {
    return null;
  }
};
