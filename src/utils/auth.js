export function authGuard(to, from, next) {
  if (localStorage.getItem("token")) {
    next();
  } else {
    next("/login");
  }
}
