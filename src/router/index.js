import { createRouter, createWebHistory } from "vue-router";
import { authGuard } from "../utils/auth";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("../pages/home/index.vue"),
      redirect: "dashboard",
      meta: {
        layout: "default",
      },
      beforeEnter: authGuard,
    },
    {
      path: "/register",
      name: "register",
      component: () => import("../pages/auth/register.vue"),
      meta: {
        layout: "default",
      },
    },
    {
      path: "/login",
      name: "login",
      component: () => import("../pages/auth/login.vue"),
      meta: {
        layout: "default",
      },
    },
    {
      path: "/dashboard",
      name: "dashboard",
      component: () => import("../pages/dashboard/index.vue"),
      meta: {
        layout: "dashboard",
      },
      beforeEnter: authGuard,
    },
  ],
});

export default router;
