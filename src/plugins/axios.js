import axios from "axios";

const $api = axios.create({
  baseURL: "http://vds.arzongina.uz:8090/api/",

  headers: {
    "Content-Type": "application/json",
  },
});

export default $api;

$api.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export const getToken = () => {
  const token = localStorage.getItem("token");
  if (token) {
    $api.defaults.headers.common.Authorization = `Bearer ${token}`;
  }
};

getToken();
